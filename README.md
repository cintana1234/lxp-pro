# DevdacticMulti

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.22.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


go to project folder and execute command
ionic start appOne blank

run the ionic multiple app
---------------------------
ionic serve --project=appOne
add platform 
---------------------
ionic cordova platform add android  --project=appOne

reuse library/ component 
------------------------------
ng g library academyLib --prefix=ac
if we change any library file
----------------------------------
ng build academyLib --watch

make web application---
----------------------------
ng g application academyweb --routing --style scss

run the web app
--------------
ng serve --project=academyweb --open