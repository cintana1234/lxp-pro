import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AcademyLibModule} from '../../../academy-lib/src/lib/academy-lib.module';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes),AcademyLibModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
