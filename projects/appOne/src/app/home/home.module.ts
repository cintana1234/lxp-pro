import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import {AcademyLibModule} from '../../../../../projects/academy-lib/src/lib/academy-lib.module';
import { HomePage } from './home.page';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    AcademyLibModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
